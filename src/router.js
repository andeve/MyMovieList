import React from 'react'
import { HashRouter as Router, Switch, Route } from 'react-router-dom'

import AppLayout from './layouts'
import Homepage from './containers/Homepage'
import Search from './containers/Search'
import Details from './containers/Details'

/**
 *
 * @param layout
 * @param component
 * @param rest
 * @returns {*}
 * @constructor
 */
const RouteWithLayout = ({layout, component, ...rest}) => {
  return (
    <Route {...rest} render={(props) =>
      React.createElement(
        layout,
        props,
        React.createElement(component, props)
      )
    }/>
  )
}

// Initialisation de l'application
const AppRouter = (
  <Router >
    <Switch>
      <RouteWithLayout exact path='/' layout={AppLayout} component={Homepage}/>
      <RouteWithLayout path='/search/:type_of_content(movie|serie)' layout={AppLayout} component={Search}/>
      <RouteWithLayout path='/category/:type_of_content(movie|serie)/:category' layout={AppLayout} component={Search}/>
      <RouteWithLayout path='/:type_of_content(movie|serie)/:id(\d+)' layout={AppLayout} component={Details}/>
    </Switch>
  </Router>
)

export default AppRouter
