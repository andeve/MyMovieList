import { combineReducers } from 'redux'

import DiscoverReducers from './DiscoverReducers'
import GenreReducers from './GenreReducers'
import VisualItemReducers from './VisualItemReducers'

export default combineReducers({
  discover: DiscoverReducers,
  genres: GenreReducers,
  visual_item: VisualItemReducers 
})
