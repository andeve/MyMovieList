import {
  API_MOVIE_DETAILS_PENDING,
  API_MOVIE_DETAILS_FULFILLED,
  API_MOVIE_DETAILS_REJECTED,
  API_SERIE_DETAILS_PENDING,
  API_SERIE_DETAILS_FULFILLED,
  API_SERIE_DETAILS_REJECTED
} from '../actions/VisualItemAction'

const initialState = {
  details: {
    pending: false,
    success: false,
    error: null,
    content: {}
  }
}

export default function VisualItemReducers(state = initialState, action) {
  const { payload, type } = action

  switch (type) {
  case API_MOVIE_DETAILS_PENDING:
  case API_SERIE_DETAILS_PENDING: {
    return {
      ...state,
      details: {
        pending: true,
        success: false,
        error: null,
        content: {}
      }
    }
  }

  case API_MOVIE_DETAILS_REJECTED:
  case API_SERIE_DETAILS_REJECTED: {
    return {
      ...state,
      details: {
        pending: false,
        success: false,
        error: payload,
        content: {}
      }
    }
  }

  case API_MOVIE_DETAILS_FULFILLED:
  case API_SERIE_DETAILS_FULFILLED: {
    return {
      ...state,
      details: {
        pending: false,
        success: true,
        error: null,
        content: payload
      }
    }
  }

  default:
    return state
  }
}
