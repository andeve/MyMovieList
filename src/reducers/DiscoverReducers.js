import {
  API_NEWS_MOVIE_PENDING,
  API_NEWS_MOVIE_FULFILLED,
  API_NEWS_MOVIE_REJECTED,
  API_NEWS_SERIE_PENDING,
  API_NEWS_SERIE_FULFILLED,
  API_NEWS_SERIE_REJECTED,

  API_SEARCH_MOVIE_PENDING,
  API_SEARCH_MOVIE_FULFILLED,
  API_SEARCH_MOVIE_REJECTED,
  API_SEARCH_SERIE_PENDING,
  API_SEARCH_SERIE_FULFILLED,
  API_SEARCH_SERIE_REJECTED,
} from '../actions/DiscoverAction'


const defaultContent = {
  pending: false,
  success: false,
  error: null,
  content: []
}

const defaultContentTypeState = {
  movie: {
    ...defaultContent
  },
  serie: {
    ...defaultContent
  }
}

const initialState = {
  news: {
    ...defaultContentTypeState
  },
  search: {
    form: {

    },
    list: [],
    ...defaultContentTypeState
  }
}

export default function DiscoverReducers(state = initialState, action) {
  const { payload, type, meta } = action

  switch (type) {

  case API_NEWS_MOVIE_PENDING :
  case API_NEWS_SERIE_PENDING :
  case API_SEARCH_MOVIE_PENDING :
  case API_SEARCH_SERIE_PENDING : {
    return {
      ...state,
      [meta.action_page]: {
        ...state[meta.action_page],
        [meta.type_of_content]: {
          pending: true,
          success: false,
          error: null,
          content: []
        }
      }
    }
  }

  case API_NEWS_MOVIE_REJECTED :
  case API_NEWS_SERIE_REJECTED :
  case API_SEARCH_MOVIE_REJECTED :
  case API_SEARCH_SERIE_REJECTED : {
    return {
      ...state,
      [meta.action_page]: {
        ...state[meta.action_page],
        [meta.type_of_content]: {
          pending: false,
          success: false,
          error: payload,
          content: []
        }
      }
    }
  }

  case API_NEWS_MOVIE_FULFILLED :
  case API_NEWS_SERIE_FULFILLED :
  case API_SEARCH_MOVIE_FULFILLED :
  case API_SEARCH_SERIE_FULFILLED : {
    return {
      ...state,
      [meta.action_page]: {
        ...state[meta.action_page],
        [meta.type_of_content]: {
          pending: false,
          success: true,
          error: null,
          content: payload
        }
      }
    }
  }

  default:
    return state
  }
}
