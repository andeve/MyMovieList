import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  Container,
  Row,
  Col
} from 'reactstrap'

import ResultsSearchList from '../components/organisms/ResultsSearchList'

import { getSearchItems } from '../selectors/DiscoveredItems'

import { getDiscover } from '../actions/DiscoverAction'

import { getCurrentDate, getCurrentYear, getCurrentDateMinusMonth } from '../services/helpers/DateHelper'
import { getRouteParam, getCurrentRoute } from '../services/helpers/RouterHelper'
import { inArray } from '../services/helpers/UtilHelper'

const availablesPages = {
  '/search/movie': {
    title: 'Découvrez de nouveaux films',
    queryParameters: {
      sort_by: 'popularity.desc',
      primary_release_year: getCurrentYear()
    }
  },
  '/search/serie': {
    title: 'Découvrez de nouvelles séries',
    queryParameters: {
      sort_by: 'popularity.desc',
      first_air_date_year: getCurrentYear()
    }
  },
  '/category/movie/popular': {
    title: 'Films populaires',
    queryParameters: {
      sort_by: 'popularity.desc',
      primary_release_year: getCurrentYear()
    }
  },
  '/category/movie/top-rated': {
    title: 'Films les mieux notés',
    queryParameters: {
      sort_by: 'vote_average.desc',
      'vote_count.gte': 1000
    }
  },
  '/category/movie/upcoming': {
    title: 'Prochainement',
    queryParameters: {
      sort_by: 'popularity.desc',
      primary_release_year: getCurrentYear(),
      'primary_release_date.gte': getCurrentDate()
    }
  },
  '/category/movie/now-playing': {
    title: 'Films actuellement en salles',
    queryParameters: {
      sort_by: 'popularity.desc',
      'release_date.gte': getCurrentDateMinusMonth(1),
      'release_date.lte': getCurrentDate()
    }
  },
  '/category/serie/popular': {
    title: 'Découvrez de nouvelles séries',
    queryParameters: {
      sort_by: 'popularity.desc',
      first_air_date_year: getCurrentYear()
    }
  },
  '/category/serie/top-rated': {
    title: 'Séries les mieux notées',
    queryParameters: {
      sort_by: 'vote_average.desc',
      'vote_count.gte': 1000
    }
  },
  '/category/serie/on-the-air': {
    title: 'Séries en cours de diffusion',
    queryParameters: {
      sort_by: 'popularity.desc',
      'first_air_date.gte': getCurrentDateMinusMonth(1),
      'first_air_date.lte': getCurrentDate()
    }
  }
}

class Search extends React.Component {
  componentDidMount() {
    const {
      selectedPage: { queryParameters },
      type_of_content,
      dispatch
    } = this.props

    dispatch(getDiscover(type_of_content, 'search', queryParameters))
  }

  componentDidUpdate(prevProps) {
    const {
      selectedPage: { queryParameters },
      type_of_content,
      dispatch
    } = this.props

    if(getCurrentRoute(this.props) !== getCurrentRoute(prevProps)) {
      dispatch(getDiscover(type_of_content, 'search', queryParameters))
    }
  }

  render() {
    const {
      selectedPage: { title },
      type_of_content,
      results
    } = this.props

    return (
      <Container>

        <h1 className='my-4'>
          {title}
        </h1>

        <Row>
          <Col>
            {!_.isEmpty(results) && <ResultsSearchList
              type={type_of_content}
              results={results}
            />}
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = (state, props) => {
  const currentUrl = getCurrentRoute(props)
  const action_name = currentUrl.split('/')[1]
  const type_of_content = getRouteParam(props, 'type_of_content')
  const category = getRouteParam(props, 'category')

  // Check the validity of the requested URL
  if(!inArray(_.keys(availablesPages), currentUrl)) {
    props.history.push('/404')
  }

  return {
    action_name: action_name,
    type_of_content: type_of_content,
    category: category,
    selectedPage: availablesPages[currentUrl],
    results: getSearchItems(state, {type: type_of_content})
  }
}

export default connect(mapStateToProps)(Search)
