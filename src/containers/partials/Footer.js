import React from 'react'
import { Container } from 'reactstrap'

const Footer = () => (
  <footer className='py-5 bg-dark'>
    <Container>
      <p className='m-0 text-center text-white'>Copyright &copy; Your Website 2018</p>
    </Container>
  </footer>
)

export default Footer
