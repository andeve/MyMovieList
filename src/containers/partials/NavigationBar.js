import React from 'react'
import {
  Collapse,
  Container,
  Button,
  Form,
  Col,
  Input,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class NavigationBar extends React.Component {
  /**
   * @constructor
   * @param props
   */
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false
    }
  }

  /**
   * Manage the responsive menu when the device need it
   * @returns {void}
   */
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  /**
   * Display left panel from menu
   * @returns {JSX}
   */
  renderLeftMenu() {
    return (
      <Nav navbar>

        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>Découvrir</DropdownToggle>
          <DropdownMenu>
            <DropdownItem tag={Link} to='/search/movie'>Films</DropdownItem>
            <DropdownItem tag={Link} to='/search/serie'>Séries</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>

        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>Films</DropdownToggle>
          <DropdownMenu>
            <DropdownItem tag={Link} to='/category/movie/popular'>Populaires</DropdownItem>
            <DropdownItem tag={Link} to='/category/movie/top-rated'>Mieux notés</DropdownItem>
            <DropdownItem tag={Link} to='/category/movie/upcoming'>Prochainement</DropdownItem>
            <DropdownItem tag={Link} to='/category/movie/now-playing'>En salles</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>

        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>Séries</DropdownToggle>
          <DropdownMenu>
            <DropdownItem tag={Link} to='/category/serie/popular'>Populaires</DropdownItem>
            <DropdownItem tag={Link} to='/category/serie/top-rated'>Mieux notées</DropdownItem>
            <DropdownItem tag={Link} to='/category/serie/on-the-air'>En cours</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>

      </Nav>
    )
  }

  /**
   * Display search bar for menu
   * @returns {JSX}
   */
  renderSearchBar() {
    return (
      <Form className='form-inline my-2 col-lg-7'>

        <Col className='input-group'>
          <Input type='text' name='search' placeholder='Rechercher un film, une série, un acteur, ...' />
          <span className='input-group-btn' style={{marginRight: '-2px'}}>
            <Button color='info'>
              <FontAwesomeIcon icon='search' />
            </Button>
          </span>
        </Col>

      </Form>
    )
  }

  /**
   * Display right panel for menu
   * @returns {JSX}
   */
  renderRightMenu() {
    return (
      <Nav className='ml-auto' navbar>

        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>FR</DropdownToggle>
          <DropdownMenu>
            <DropdownItem>FR</DropdownItem>
            <DropdownItem>EN</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>

        <NavItem>
          <NavLink tag={Link} to='/login'>Connexion</NavLink>
        </NavItem>

      </Nav>
    )
  }

  /**
   * Main render
   * @returns {JSX}
   */
  render() {
    return (
      <Navbar color='dark' dark expand='lg' fixed='top'>
        <Container>
          <NavbarBrand tag={Link} to='/'>
            <img src='http://placehold.it/81x72' width='81' height='72' alt='logo' />
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>

            {this.renderLeftMenu()}
            {this.renderSearchBar()}
            {this.renderRightMenu()}

          </Collapse>
        </Container>
      </Navbar>
    )
  }
}

export default NavigationBar
