import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardImg,
  CardText
} from 'reactstrap'

import { getRouteParam } from '../services/helpers/RouterHelper'
import { getYear } from '../services/helpers/DateHelper'
import { buildImgUrl } from '../services/helpers/ConfigHelper'

import { buildItemToDisplay } from '../selectors/DiscoveredItems'

import { getDetails } from '../actions/VisualItemAction'

import PersonFilteredList from '../components/organisms/PersonFilteredList'
import MoreInformation from '../components/organisms/MoreInformation'
import VisualItemSlider from '../components/organisms/VisualItemSlider'
import AnchorNav from '../components/molecules/AnchorNav'
import PersonMedia from '../components/molecules/PersonMedia'

class Details extends React.Component {
  componentDidMount() {
    const { dispatch, type_of_content, visual_item_id } = this.props

    dispatch(getDetails(type_of_content, visual_item_id))
  }

  componentDidUpdate(prevProps) {
    const { dispatch, type_of_content } = this.props

    if(prevProps.visual_item_id !== this.props.visual_item_id) {
      dispatch(getDetails(type_of_content, this.props.visual_item_id))
    }
  }

  render() {
    const { visual_item, type_of_content } = this.props
    const ready_to_display = !_.isEmpty(visual_item)

    const item = buildItemToDisplay(visual_item)
    const hasCreators = !_.isEmpty(item.created_by)
    const hasCollection = !_.isEmpty(item.belongs_to_collection)
    const hasSeasons = _.isNumber(item.number_of_seasons)
    const hasRecommendations = !_.isEmpty(item.recommendations) && !_.isEmpty(item.recommendations.results)

    const anchors = [
      { link: '#casting', label: 'Acteurs', display: true },
      { link: '#production', label: 'Production', display: true },
      { link: '#collection', label: 'Collection', display: hasCollection },
      { link: '#seasons', label: 'Saisons', display: hasSeasons },
      { link: '#recommendations', label: 'Recommandations', display: hasRecommendations }
    ]

    return (
      <Container>

        {ready_to_display && <div>

          <Row className='pb-2'>
            <Card style={{width: '100%'}}>
              <Row>
                <Col md='4'>
                  <CardImg top src={buildImgUrl(item.poster_path, 'poster', 400)} />
                </Col>

                <Col md='8'>
                  <CardBody className='px-3'>
                    <CardTitle tag={'h3'}>{item.title} <small>({getYear(item.release_date)})</small></CardTitle>
                  </CardBody>

                  <CardBody className='px-3'>
                    {'--> Here will be the rating widget and users actions <--'}
                  </CardBody>

                  <CardBody className='px-3'>
                    <CardTitle>Synopsis</CardTitle>
                    <CardText>{item.description}</CardText>
                  </CardBody>

                  {hasCreators && <CardBody className='px-3'>
                    <CardTitle>Créateurs</CardTitle>
                    <ul id='creators'>
                      {_.map(item.created_by, (creator, index) => (
                        <li key={index}>
                          <PersonMedia type='creator' person={creator} />
                        </li>
                      ))}
                    </ul>
                  </CardBody>}
                </Col>

              </Row>
            </Card>
          </Row>

          <Row>
            <Col lg='9' md='12'>

              <AnchorNav anchors={anchors} />

              <div id='casting'>
                <PersonFilteredList
                  title='Acteurs principaux'
                  type='cast'
                  persons={item.credits}
                />
              </div>

              <div id='production'>
                <PersonFilteredList
                  title='Production'
                  type='crew'
                  persons={item.credits}
                />
              </div>

              {hasCollection && <div id='collection'>
                <VisualItemSlider
                  title={'Fait partie de ' + item.belongs_to_collection.name}
                  visualItems={item.belongs_to_collection.parts}
                  type={type_of_content}
                />
              </div>}

              {hasSeasons && <div id='seasons'>
                <VisualItemSlider
                  title='Saisons'
                  visualItems={item.seasons}
                  type={type_of_content}
                  kind_of_image='poster'
                />
              </div>}

              {hasRecommendations && <div id='recommendations'>
                <VisualItemSlider
                  title='Recommandations'
                  visualItems={item.recommendations.results}
                  type={type_of_content}
                />
              </div>}

            </Col>

            <Col lg='3' md='12'>
              <MoreInformation item={item} type={type_of_content} />
            </Col>
          </Row>

        </div>}

      </Container>
    )
  }
}

const mapStateToProps = (state, props) => {
  const type_of_content = getRouteParam(props, 'type_of_content')
  const visual_item_id = getRouteParam(props, 'id')

  return {
    type_of_content: type_of_content,
    visual_item_id: visual_item_id,
    visual_item: state.visual_item.details.content
  }
}

export default connect(mapStateToProps)(Details)
