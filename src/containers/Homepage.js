import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  Container,
  Row,
  Col
} from 'reactstrap'

import TopicsList from '../components/organisms/TopicsList'
import GenresWidget from "../components/organisms/GenresWidget"
import TopRatedWidget from "../components/organisms/TopRatedWidget"

import { getNewsItems } from '../selectors/DiscoveredItems'

import { getDiscover } from '../actions/DiscoverAction'

import { getCurrentYear } from '../services/helpers/DateHelper'

class Homepage extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props

    dispatch(getDiscover('movie', 'news', {sort_by: 'popularity.desc', primary_release_year: getCurrentYear()}))
    dispatch(getDiscover('serie', 'news', {sort_by: 'popularity.desc', first_air_date_year: getCurrentYear()}))
  }

  render() {
    const {
      discovered_movies,
      discovered_series
    } = this.props

    return (
      <Container fluid>
        <Row>
          <Col md='9'>
            {!_.isEmpty(discovered_movies) && <TopicsList
              title='Films'
              subtitle='actualités'
              type='movie'
              topics={discovered_movies}
            />}
            {!_.isEmpty(discovered_series) && <TopicsList
              title='Séries'
              subtitle='actualités'
              type='serie'
              topics={discovered_series}
            />}
          </Col>
          <Col md='3'>
            <GenresWidget />
            <TopRatedWidget />
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    discovered_movies: getNewsItems(state, {type: 'movie'}),
    discovered_series: getNewsItems(state, {type: 'serie'})
  }
}

export default connect(mapStateToProps)(Homepage)
