const getRouteParamsFromProps = function (props) {
  return props.match.params
}

export const getRouteParam = function (props, param_name) {
  return getRouteParamsFromProps(props).hasOwnProperty(param_name) ? getRouteParamsFromProps(props)[param_name] : null
}

export const getCurrentRoute = function(props) {
  return props.location.pathname
}