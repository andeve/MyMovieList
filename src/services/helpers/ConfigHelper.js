import { config } from '../../config'

import profileNoImage45 from '../../resources/images/profile/no-image-w45.jpg'
import profileNoImage185 from '../../resources/images/profile/no-image-w185.jpg'
import backdropNoImage300 from '../../resources/images/backdrop/no-image-w300.jpg'
import posterNoImage300 from '../../resources/images/poster/no-image-w300.jpg'
import posterNoImage400 from '../../resources/images/poster/no-image-w400.jpg'

const buildImgUrlProfile = function (size) {
  switch (size) {
    case 45: return profileNoImage45
    case 185: return profileNoImage185
    default: return `Image size (${size}) for profiles`
  }
}

const buildImgUrlPoster = function (size) {
  switch (size) {
    case 300: return posterNoImage300
    case 400: return posterNoImage400
    default: return `Image size (${size}) for posters`
  }
}

const buildImgUrlBackdrop = function (size) {
  switch (size) {
    case 300: return backdropNoImage300
    default: return `Image size (${size}) for backdrops`
  }
}

/**
 * Build and return the full image link
 * @param {string} image_name : image name returns by all movies/series API
 * @param {string} type_of_image : type of the image (usefull when image is not exist)
 * @param {integer} size : image width
 * @returns {string} full image link
 */
export const buildImgUrl = function (image_name, type_of_image, size = 400) {
  if (!_.isEmpty(image_name)) {
    return config.API_TMDB_IMG_URL + `w${size}` + image_name
  }

  switch (type_of_image) {
    case 'backdrop': return buildImgUrlBackdrop(size)
    case 'poster': return buildImgUrlPoster(size)
    case 'profile': return buildImgUrlProfile(size)
    default: return `Image type (${type_of_image}) is not supported`
  }
}
