import _ from 'lodash'

export const inArray = function (availables_values, value) {
  return _.indexOf(availables_values, value) !== -1
}

export const formatBigNumber = function (number, locale = 'fr-FR') {
  return new Intl.NumberFormat(locale, { style: 'currency', currency: 'USD', currencyDisplay: 'symbol' }).format(number)
}