import moment from 'moment'
import _ from 'lodash'

export const getCurrentDate = function () {
  return moment().format('YYYY-MM-DD')
}

export const getCurrentYear = function() {
  return moment().format('YYYY')
}

export const getYear = function(date) {
  return moment(date).format('YYYY')
}

export const getCurrentDateMinusMonth = function(minus) {
  return moment().subtract(minus, 'months').format('YYYY-MM-DD')
}

export const toFR = function(date) {
  if(_.isEmpty(date)) {
    return ''
  }

  return moment(date).format('DD/MM/YYYY')
}
