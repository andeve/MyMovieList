import React from 'react'
import PropTypes from 'prop-types'
import ReactRating from 'react-rating'
import ReactTooltip from 'react-tooltip'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Rating extends React.Component {
  render() {
    const { vote, readonly } = this.props
    let rate = this.props.rate / 2

    return (
      <div data-tip={`${rate} / 5 (${vote} votes)`}>
        <ReactRating 
          initialRating={rate}
          stop={5}
          fractions={5}
          readonly={readonly}
          emptySymbol={<FontAwesomeIcon icon={['far','star']} />}
          fullSymbol={<FontAwesomeIcon icon='star' />}
        />
        <ReactTooltip />
      </div>
    )
  }
}

Rating.propTypes = {
  rate: PropTypes.number.isRequired,
  vote: PropTypes.number.isRequired,
  readonly: PropTypes.bool
}

Rating.defaultProps = {
  readonly: true
}

export default Rating
