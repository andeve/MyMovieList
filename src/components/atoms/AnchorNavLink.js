import React from 'react'
import { HashLink as Link } from 'react-router-hash-link'
import { NavLink } from 'reactstrap'

const AnchorNavLink = ({label, link}) => (
  <NavLink tag={Link} to={link} scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'center' })}>{label}</NavLink>
)

export default AnchorNavLink
