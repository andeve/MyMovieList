import React from 'react'
import _ from 'lodash'
import {
  Row,
  Badge
} from 'reactstrap'

import { toFR } from '../../services/helpers/DateHelper'
import { formatBigNumber } from '../../services/helpers/UtilHelper'

class MoreInformation extends React.Component {
  renderMovieInformation() {
    const { item } = this.props

    const lines = [
      { label: 'Titre original', value: item.original_title },
      { label: 'Statut', value: item.status },
      { label: 'Sortie cinéma', value: toFR(item.release_date) },
      { label: 'Langue originale', value: item.original_language },
      { label: 'Durée', value: `${item.runtime} min` },
      { label: 'Budget', value: formatBigNumber(item.budget) },
      { label: 'Recettes', value: formatBigNumber(item.revenue) },
      { label: 'Site web', value: <a href={item.homepage} target='_blank'>{item.homepage}</a> },
      { label: 'Genres', value: _.map(item.genres, (genre, index) => (<Badge key={index} color='info' className='mr-1'>{genre.name}</Badge>)) },
      { label: 'Mots-clés', value: _.map(item.keywords.keywords, (keyword, index) => (<Badge key={index} color='info' className='mr-1'>{keyword.name}</Badge>)) }
    ]

    return _.map(lines, (line, index) => ( this.renderLine(line, index) ))
  }

  renderSerieInformation() {
    const { item } = this.props

    const lines = [
      { label: 'Statut', value: item.status },
      { label: 'Première diffusion', value: toFR(item.first_air_date) },
      { label: 'Langue originale', value: item.original_language },
      { label: 'Saisons', value: item.number_of_seasons },
      { label: 'Episodes', value: item.number_of_episodes },
      { label: 'Durée des épisodes', value: `${item.episode_run_time} min` },
      { label: 'Site web', value: <a href={item.homepage} target='_blank'>{item.homepage}</a> },
      { label: 'Genres', value: _.map(item.genres, (genre, index) => (<Badge key={index} color='info' className='mr-1'>{genre.name}</Badge>)) },
      { label: 'Mots-clés', value: _.map(item.keywords.results, (keyword, index) => (<Badge key={index} color='info' className='mr-1'>{keyword.name}</Badge>)) }
    ]

    return _.map(lines, (line, index) => ( this.renderLine(line, index) ))
  }

  renderLine(line, index) {
    return (
      <p key={index}>
        <strong>{line.label} : </strong>{line.value}
      </p>
    )
  }

  render() {
    const { type } = this.props

    return (
      <Row id='moreInformation' className='py-2 pl-4'>
        <h4>Informations</h4>

        {type === 'movie' && this.renderMovieInformation()}
        {type === 'serie' && this.renderSerieInformation()}
      </Row>
    )
  }
}

export default MoreInformation
