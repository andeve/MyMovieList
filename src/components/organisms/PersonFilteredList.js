import React from 'react'
import {
  Row,
  CardGroup
} from 'reactstrap'

import PersonsModal from '../molecules/PersonsModal'

import PersonMedia from '../molecules/PersonMedia'
import PersonCard from '../molecules/PersonCard'

class PersonFilteredList extends React.Component {
  renderPersons() {
    const { type, persons } = this.props

    return _.map(_.range(0, _.min([persons[type].length, 5])), (iteration, index) => (
      <PersonCard key={index} type={type} person={persons[type][iteration]} />
    ))
  }

  renderModalPersons() {
    const { persons, type } = this.props

    return _.map(persons[type], (person, index) => (
      <PersonMedia key={index} type={type} person={person} />
    ))
  }

  render() {
    const { title, persons, type } = this.props

    return (
      <Row className='py-2'>
        <h4>{title}</h4>

        <CardGroup>
          {this.renderPersons()}
        </CardGroup>

        <PersonsModal
          openButtonLabel={'Voir plus (' + persons[type].length + ')'}
          header={title}
          content={this.renderModalPersons()}
          closeButtonLabel='Fermer'
        />

      </Row>
    )
  }
}

export default PersonFilteredList
