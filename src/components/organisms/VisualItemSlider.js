import React from 'react'
import {
  Row,
  CardGroup
} from 'reactstrap'
import PropTypes from 'prop-types'

import VerticalVisualItemCard from "../molecules/VerticalVisualItemCard";

class VisualItemSlider extends React.Component {
  renderVisualItem() {
    const { type, visualItems, kind_of_image } = this.props

    return _.map(visualItems, (visualItem, index) => (
      <VerticalVisualItemCard
        key={index}
        item={visualItem}
        type={type}
        image_size={300}
        displayOverlay={false}
        kind_of_image={kind_of_image}
      />
    ))
  }

  render() {
    const { title } = this.props

    return (
      <Row className='py-2'>
        <h4>{title}</h4>

        <CardGroup className='slider'>
          {this.renderVisualItem()}
        </CardGroup>

      </Row>
    )
  }
}

VisualItemSlider.propTypes = {
  visualItems: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
  kind_of_image: PropTypes.string.isRequired
}

VisualItemSlider.defaultProps = {
  kind_of_image: 'backdrop'
}

export default VisualItemSlider
