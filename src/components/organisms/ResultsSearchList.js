import React from 'react'
import _ from 'lodash'
import PropTypes from 'prop-types'
import {
  Row,
  Col
} from 'reactstrap'

import HorizontalVisualItemCard from '../molecules/HorizontalVisualItemCard'

class ResultsSearchList extends React.Component {

  render() {
    const {
      type,
      results
    } = this.props

    return (
      <div>

        <Row>
          {_.map(results, (element, index) => (
            <Col md='6' lg='6' className='mb-4' key={index} >
              <HorizontalVisualItemCard item={element} type={type}/>
            </Col>
          ))}
        </Row>

      </div>
    )
  }
}

ResultsSearchList.propTypes = {
  type: PropTypes.string.isRequired,
  results: PropTypes.array.isRequired
}

export default ResultsSearchList
