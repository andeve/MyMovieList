import React from 'react'
import { Link } from 'react-router-dom'
import { Media } from 'reactstrap'

import { buildImgUrl } from '../../services/helpers/ConfigHelper'

class PersonMedia extends React.Component {
  renderJob() {
    const { person, type } = this.props

    switch(type) {
      case 'cast': return person.character
      case 'crew': return person.job
      case 'creator' : return 'Créateur'
      default: return ''
    }
  }

  render() {
    const { person } = this.props

    return (
      <Media className='py-2'>
        <Media left tag={Link} to={`/person/${person.id}`}>
          <Media object src={buildImgUrl(person.profile_path, 'profile', 45)} />
        </Media>
        <Media body className='pl-2'>
          <Media tag={Link} to={`/person/${person.id}`}>
            <Media heading>{person.name}</Media>
          </Media>
          {this.renderJob()}
        </Media>
      </Media>
    )
  }
}

export default PersonMedia
