import React from 'react'
import { Link } from 'react-router-dom'
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardImg,
  CardLink
} from 'reactstrap'

import { buildImgUrl } from '../../services/helpers/ConfigHelper'

class PersonCard extends React.Component {
  renderJob() {
    const { person, type } = this.props

    switch(type) {
      case 'cast': return person.character
      case 'crew': return person.job
      case 'creator' : return 'Créateur'
      default: return ''
    }
  }

  render() {
    const { person } = this.props

    return (
      <Card>
        <CardLink tag={Link} to={`/person/${person.id}`}>
          <CardImg top src={buildImgUrl(person.profile_path, 'profile', 185)} />
        </CardLink>
        <CardBody>
          <CardTitle>
            <CardLink tag={Link} to={`/person/${person.id}`}>{person.name}</CardLink>
          </CardTitle>
          <CardSubtitle>{this.renderJob()}</CardSubtitle>
        </CardBody>
      </Card>
    )
  }
}

export default PersonCard
