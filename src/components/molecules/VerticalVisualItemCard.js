import React from 'react'
import PropTypes from 'prop-types'
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardBody,
  CardLink,
  CardTitle,
  CardSubtitle,
  CardText
} from 'reactstrap'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import Rating from '../atoms/Rating'

import { buildImgUrl } from '../../services/helpers/ConfigHelper'

import { buildItemToDisplay } from '../../selectors/DiscoveredItems'

class VerticalVisualItemCard extends React.Component {
  render() {
    const { item, type, image_size, displayOverlay, kind_of_image } = this.props

    let cardItem = buildItemToDisplay(item)

    return (
      <Card>
        <CardLink tag={Link} to={`/${type}/${item.id}`}>
          <CardImg top src={buildImgUrl(cardItem[`${kind_of_image}_path`], kind_of_image, image_size)} />
          {displayOverlay && <CardImgOverlay>
            <CardTitle className='limited-oneline'>{cardItem.title}</CardTitle>
            <CardText>{cardItem.description}</CardText>
          </CardImgOverlay>}
        </CardLink>
        <CardBody>
          <CardTitle className='limited-oneline'>
            <CardLink tag={Link} to={`/${type}/${item.id}`}>{cardItem.title}</CardLink>
          </CardTitle>
          <CardSubtitle>
            {_.isNumber(cardItem.rate) && <Rating rate={cardItem.rate} vote={cardItem.vote} />}
            {_.isNumber(cardItem.episode_count) ? `Nombre d\'épisodes : ${cardItem.episode_count}` : ''}
          </CardSubtitle>
        </CardBody>
      </Card>
    )
  }
}

VerticalVisualItemCard.propTypes = {
  item: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  image_size: PropTypes.number.isRequired,
  displayOverlay: PropTypes.bool.isRequired,
  kind_of_image: PropTypes.string.isRequired
}

VerticalVisualItemCard.defaultProps = {
  displayOverlay: true
}

export default VerticalVisualItemCard
