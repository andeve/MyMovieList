import React from 'react'
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap'

class PersonsModal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      modal: false
    }

    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    })
  }

  render() {
    const {
      openButtonLabel,
      header,
      content,
      closeButtonLabel
    } = this.props

    return (
      <div className='full-width'>
        <Button color='info' className='full-width' onClick={this.toggle}>{openButtonLabel}</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>{header}</ModalHeader>
          <ModalBody>{content}</ModalBody>
          <ModalFooter>
            <Button color='primary' onClick={this.toggle}>{closeButtonLabel}</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

export default PersonsModal
