import React from 'react'
import { Nav, NavItem } from 'reactstrap'

import AnchorNavLink from '../atoms/AnchorNavLink'

const AnchorNav = ({anchors}) => (
  <Nav justified>
    {_.map(anchors, (anchor, index) => (
      <NavItem key={index} className={!anchor.display ? 'd-none' : ''}>
        <AnchorNavLink link={anchor.link} label={anchor.label} />
      </NavItem>
    ))}
  </Nav>
)

export default AnchorNav
