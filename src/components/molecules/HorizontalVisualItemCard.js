import React from 'react'
import PropTypes from 'prop-types'
import {
  Card,
  CardImg,
  CardBody,
  CardLink,
  CardTitle,
  CardSubtitle,
  CardText,
  Row,
  Col
} from 'reactstrap'
import { Link } from 'react-router-dom'

import Rating from '../atoms/Rating'

import { buildItemToDisplay } from '../../selectors/DiscoveredItems'

import { buildImgUrl } from '../../services/helpers/ConfigHelper'

class HorizontalVisualItemCard extends React.Component {
  render() {
    const { item, type } = this.props

    let cardItem = buildItemToDisplay(item)

    return (
      <Card>
        <Row>
          <Col md='4'>
            <CardLink tag={Link} to={`/${type}/${cardItem.id}`}>
              <CardImg top src={buildImgUrl(cardItem.poster_path, 'poster', 400)} />
            </CardLink>
          </Col>

          <Col md='8' style={{paddingLeft: 0}} >
            <CardBody className='px-3'>
              <CardTitle className='limited-oneline'>
                <CardLink tag={Link} to={`/${type}/${cardItem.id}`}>{cardItem.title}</CardLink>
              </CardTitle>
              <CardSubtitle>
                <Rating rate={cardItem.rate} vote={cardItem.vote} />
              </CardSubtitle>
              <CardText className='limited-multiline pt-2'>{cardItem.description}</CardText>
            </CardBody>
          </Col>

        </Row>
      </Card>
    )
  }
}

HorizontalVisualItemCard.propTypes = {
  type: PropTypes.string.isRequired,
  item: PropTypes.object.isRequired
}

export default HorizontalVisualItemCard
