import { createSelector } from 'reselect'

const getNewsList = (state, props) => {
  return state.discover.news[props.type].content
}

const getSearchList = (state, props) => {
  return state.discover.search[props.type].content
}

export const getNewsItems = createSelector(
  getNewsList,
  (newsList) => {
    return newsList
  }
)

export const getSearchItems = createSelector(
  getSearchList,
  (resultsList) => {
    return resultsList
  }
)

export const buildItemToDisplay = (item) => {
  return {
    ...item,
    id: item.id,
    title: !_.isEmpty(item.title) ? item.title : item.name,
    description: item.overview,
    rate: item.vote_average,
    vote: item.vote_count
  }
}
