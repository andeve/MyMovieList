import _ from 'lodash'

import { get, getTypeOfContent } from '../services/interfaces/ApiTMDB'

/* eslint-disable */

export const API_MOVIE_DETAILS            = '@@MYMOVIELIST/API_MOVIE_DETAILS'
export const API_MOVIE_DETAILS_PENDING    = '@@MYMOVIELIST/API_MOVIE_DETAILS_PENDING'
export const API_MOVIE_DETAILS_FULFILLED  = '@@MYMOVIELIST/API_MOVIE_DETAILS_FULFILLED'
export const API_MOVIE_DETAILS_REJECTED   = '@@MYMOVIELIST/API_MOVIE_DETAILS_REJECTED'

export const API_SERIE_DETAILS            = '@@MYMOVIELIST/API_SERIE_DETAILS'
export const API_SERIE_DETAILS_PENDING    = '@@MYMOVIELIST/API_SERIE_DETAILS_PENDING'
export const API_SERIE_DETAILS_FULFILLED  = '@@MYMOVIELIST/API_SERIE_DETAILS_FULFILLED'
export const API_SERIE_DETAILS_REJECTED   = '@@MYMOVIELIST/API_SERIE_DETAILS_REJECTED'

/* eslint-enable */

const defaultLanguage = {
  language: 'fr-FR'
}

let defaultParams = {
  movie: {
    ...defaultLanguage,
    append_to_response: 'credits,keywords,similar_movies,recommendations'
  },
  serie: {
    ...defaultLanguage,
    append_to_response: 'credits,keywords,similar,recommendations'
  }
}

function getCollection(type, id) {
  return get(`collection/${id}`, {
    ...defaultLanguage
  }).then(function (response) {
    return response.data
  })
}

/**
 * Get visual item details by ID
 * @param params
 * @returns {{type: *, payload: *}}
 */
export function getDetails(type, id, params = {}) {
  return {
    type: eval(`API_${type.toUpperCase()}_DETAILS`),
    payload: get(`${getTypeOfContent(type)}/${id}`, {
      ...defaultParams[type],
      ...params
    }).then(function (response) {
      const visualItemDetail = response.data || {}
      const collectionId = !_.isEmpty(visualItemDetail.belongs_to_collection) ? visualItemDetail.belongs_to_collection.id : null

      return Promise.all([
        visualItemDetail,
        !_.isNull(collectionId) ? getCollection(type, collectionId) : null
      ])
    }).then(([visualItemDetail, collection]) => {
      return {
        ...visualItemDetail,
        belongs_to_collection: !_.isEmpty(collection) ? {...collection} : null
      }
    })
  }
}
