import { get, getTypeOfContent } from '../services/interfaces/ApiTMDB'

/* eslint-disable */

export const API_NEWS_MOVIE           = '@@MYMOVIELIST/API_NEWS_MOVIE'
export const API_NEWS_MOVIE_PENDING   = '@@MYMOVIELIST/API_NEWS_MOVIE_PENDING'
export const API_NEWS_MOVIE_FULFILLED = '@@MYMOVIELIST/API_NEWS_MOVIE_FULFILLED'
export const API_NEWS_MOVIE_REJECTED  = '@@MYMOVIELIST/API_NEWS_MOVIE_REJECTED'

export const API_NEWS_SERIE           = '@@MYMOVIELIST/API_NEWS_SERIE'
export const API_NEWS_SERIE_PENDING   = '@@MYMOVIELIST/API_NEWS_SERIE_PENDING'
export const API_NEWS_SERIE_FULFILLED = '@@MYMOVIELIST/API_NEWS_SERIE_FULFILLED'
export const API_NEWS_SERIE_REJECTED  = '@@MYMOVIELIST/API_NEWS_SERIE_REJECTED'

export const API_SEARCH_MOVIE           = '@@MYMOVIELIST/API_SEARCH_MOVIE'
export const API_SEARCH_MOVIE_PENDING   = '@@MYMOVIELIST/API_SEARCH_MOVIE_PENDING'
export const API_SEARCH_MOVIE_FULFILLED = '@@MYMOVIELIST/API_SEARCH_MOVIE_FULFILLED'
export const API_SEARCH_MOVIE_REJECTED  = '@@MYMOVIELIST/API_SEARCH_MOVIE_REJECTED'

export const API_SEARCH_SERIE           = '@@MYMOVIELIST/API_SEARCH_SERIE'
export const API_SEARCH_SERIE_PENDING   = '@@MYMOVIELIST/API_SEARCH_SERIE_PENDING'
export const API_SEARCH_SERIE_FULFILLED = '@@MYMOVIELIST/API_SEARCH_SERIE_FULFILLED'
export const API_SEARCH_SERIE_REJECTED  = '@@MYMOVIELIST/API_SEARCH_SERIE_REJECTED'

/* eslint-enable */

let defaultParams = {
  movie: {
    language: 'fr-FR',
    include_adult: false,
    include_video: false,
    region: 'FR',
    page: 1
  },
  serie: {
    language: 'fr-FR',
    timezone: 'Europe/Paris',
    include_null_first_air_dates: false,
    page: 1
  }
}

let typesActions = {
  movie_search: API_SEARCH_MOVIE,
  movie_news: API_NEWS_MOVIE,
  serie_search: API_SEARCH_SERIE,
  serie_news: API_NEWS_SERIE
}

/**
 * Get the right action to dispatch depending on the page and the type of content
 * @param {string} typeOfContent : type of contents
 * @param {string} action : action name
 * @returns {*}
 */
function getTypeAction(typeOfContent, action) {
  return typesActions[`${typeOfContent}_${action}`]
}

/**
 * Get content which must be discovered
 * @param {string} typeOfContent : type of contents
 * @param {string} action : action name
 * @param {object} params : additional parameters
 * @returns {type: *, payload: *}
 */
export function getDiscover(typeOfContent, action, params = {}) {

  let uri = 'discover/' + getTypeOfContent(typeOfContent)

  return {
    type: getTypeAction(typeOfContent, action),
    meta: {
      type_of_content: typeOfContent,
      action_page: action
    },
    payload: get(uri, {
      ...defaultParams[typeOfContent],
      ...params
    }).then(function (response) {
      return response.data.results
    })
  }
}
